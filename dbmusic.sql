USE [dbmusic]
GO
/****** Object:  Table [dbo].[Artists]    Script Date: 6/17/2020 11:13:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Artists](
	[ArtistID] [int] IDENTITY(1,1) NOT NULL,
	[ArtistName] [varchar](200) NOT NULL,
	[AlbumName] [varchar](200) NOT NULL,
	[ImageURL] [varchar](200) NULL,
	[ReleaseDate] [date] NOT NULL,
	[Price] [numeric](10, 2) NOT NULL,
	[SampleURL] [varchar](200) NULL,
 CONSTRAINT [PK_Artists] PRIMARY KEY CLUSTERED 
(
	[ArtistID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
