﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FPT_API.EF;
using FPT_API.Models;

namespace FPT_API.Controllers
{
    public class ValuesController : ApiController
    {
        private static dbmusicEntities db = new dbmusicEntities();
        // GET api/values
        public List<Artists> Get()
        {
            List<Artists> artists = db.Artists.ToList();
            return artists;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public Task<HttpResponseMessage> Post([FromBody] GridArtist m)
        {
            var response = new HttpResponseMessage();
            using (var dbx = db.Database.BeginTransaction())
            {
                try
                {
                    if (m.ArtistID == 0)
                    {
                        Artists ma = new Artists();
                        ma.ArtistName = m.ArtistName;
                        ma.AlbumName = m.AlbumName;
                        ma.ReleaseDate = m.ReleaseDate;
                        ma.Price = m.Price;
                        ma.ImageURL = m.ImageURL;
                        ma.SampleURL = m.SampleURL;

                        db.Artists.Add(ma);
                        db.SaveChanges();
                        response = Request.CreateResponse(HttpStatusCode.OK, "Success");
                    }
                    else
                    {
                        int ID = Convert.ToInt32(m.ArtistID);
                        var ma = (from c in db.Artists
                                  where c.ArtistID == ID
                                  select c).FirstOrDefault();

                        if (ma != null)
                        {
                            ma.ArtistName = m.ArtistName;
                            ma.AlbumName = m.AlbumName;
                            ma.ReleaseDate = m.ReleaseDate;
                            ma.Price = m.Price;
                            ma.ImageURL = m.ImageURL;
                            ma.SampleURL = m.SampleURL;
                            db.SaveChanges();
                            response = Request.CreateResponse(HttpStatusCode.OK, "Success");
                        }
                    }
                    dbx.Commit();
                }
                catch (DbEntityValidationException e) //DbEntityValidationException
                {
                    dbx.Rollback();
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
                }
            }

            return Task.FromResult(response);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete]
        public Task<HttpResponseMessage> Delete(int id)
        {
            var response = new HttpResponseMessage();
            using (var dbx = db.Database.BeginTransaction())
            {
                try
                {
                    if (id == 0)
                    {
                        dbx.Rollback();
                        response = Request.CreateResponse(HttpStatusCode.BadRequest, "ID is not valid");
                        return Task.FromResult(response);
                    }
                    else
                    {
                        var ma = (from c in db.Artists
                                  where c.ArtistID == id
                                  select c).FirstOrDefault();
                        db.Artists.Remove(ma);
                        db.SaveChanges();
                        response = Request.CreateResponse(HttpStatusCode.OK, "Success");
                    }
                    dbx.Commit();
                }
                catch (Exception e) //DbEntityValidationException
                {
                    dbx.Rollback();
                    response = Request.CreateResponse(HttpStatusCode.BadRequest, e.ToString());
                }
            }

            return Task.FromResult(response);
        }
    }
}
