#pragma checksum "X:\Office\Library\FPT-HomeTest\FPT_Core\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "aae2a934087024ddafa2ba1b7fc9844b8950261f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(FPT_Core.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
namespace FPT_Core.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "X:\Office\Library\FPT-HomeTest\FPT_Core\Pages\_ViewImports.cshtml"
using FPT_Core;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"aae2a934087024ddafa2ba1b7fc9844b8950261f", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"479402a3e06b8aef713ad22ae858799469b501c3", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString(""), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "get", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("addForm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "POST", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("https://localhost:44330/api/values"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "X:\Office\Library\FPT-HomeTest\FPT_Core\Pages\Index.cshtml"
  
    ViewData["Title"] = "Home page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n<div class=\"row\">\r\n    <div class=\"col-md-6 section-top-addUser\">\r\n        <button type=\"button\" class=\"btn btn-outline-primary btnAdd\">Add New</button>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-md-12\">\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "aae2a934087024ddafa2ba1b7fc9844b8950261f5443", async() => {
                WriteLiteral(@"
            <table class=""table table-sm table-hover"" id=""tblArtists"">
                <thead>
                    <tr>
                        <th data-formatter=""NumberFormatter"">No.</th>
                        <th data-field=""ArtistID"" data-visible=""false"">ID</th>
                        <th data-field=""ImageURL"" data-sortable=""true"" data-formatter=""ImgFormatter"">Album Name</th>
                        <th data-field=""ArtistName"" data-sortable=""true"">Artist Name</th>
                        <th data-field=""AlbumName"" data-visible=""false"">Album Name</th>
                        <th data-field=""ReleaseDate"" data-sortable=""true"" data-formatter=""dateFormat"">Release Date</th>
                        <th data-field=""SampleURL"" data-formatter=""MusicFormatter"" data-events=""PlayEvents"">Sample Audio</th>
                        <th data-field=""Price"" data-sortable=""true"">Price</th>
                        <th data-field=""Action"" data-formatter=""ActionFormatter"" data-events=""ActionEvents"">Action</th>
 ");
                WriteLiteral("                   </tr>\r\n                </thead>\r\n            </table>\r\n        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
    </div>
</div>


<div class=""modal fade"" id=""exampleModalCenter1"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalCenterTitle"" aria-hidden=""true"">
    <div class=""modal-dialog modal-dialog-centered"" role=""document"">
        <div class=""modal-content add-usr-pop"">
            <div class=""modal-header"">
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                Artist Name : <span id=""spArtistName""></span>
                <br />
                Album Name : <span id=""spAlbumName""></span>
                <br />
                <img id=""imgAlbum"" style=""width:100%""/>
                <div id=""audioBody""></div>
            </div>
        </div>
    </div>
</div>

<div class=""modal fade"" id=""addModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalCenterTitle"" aria-hidden=""true"">");
            WriteLiteral(@"
    <div class=""modal-dialog modal-dialog-centered"" role=""document"">
        <div class=""modal-content add-usr-pop"">
            <div class=""modal-header"">
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "aae2a934087024ddafa2ba1b7fc9844b8950261f9690", async() => {
                WriteLiteral(@"
                    <fieldset>
                        <input type=""hidden"" id=""ArtistID"" name=""ArtistID"" />
                        <div class=""form-group row"">
                            <label for=""ArtistName"" class=""col-sm-4"">Artist Name</label>
                            <div class=""col-sm-8"">
                                <input class=""form-control"" id=""ArtistName"" name=""ArtistName"" type=""text"" required>
                            </div>
                        </div>

                        <div class=""form-group row"">
                            <label for=""AlbumName"" class=""col-sm-4"">Album Name</label>
                            <div class=""col-sm-8"">
                                <input class=""form-control"" id=""AlbumName"" name=""AlbumName"" type=""text"" required>
                            </div>
                        </div>

                        <div class=""form-group row"">
                            <label for=""Price"" class=""col-sm-4"">Price</label>
                ");
                WriteLiteral(@"            <div class=""col-sm-8"">
                                <input class=""form-control"" id=""Price"" name=""Price"" type=""number"" min=""0"" value=""0.00"" step=""0.01"" required>
                            </div>
                        </div>

                        <div class=""form-group row"">
                            <label for=""ReleaseDate"" class=""col-sm-4"">Release Date</label>
                            <div class=""col-sm-8"" style=""position: relative; padding-right:30px;"">
                                <input type=""text"" class=""form-control datepicker"" id=""ReleaseDate"" name=""ReleaseDate"" required><i class=""fa fa-calendar fa-2x"" aria-hidden=""true"" style=""position: absolute; right: 1.5em; top: 2px;""></i>
                            </div>
                        </div>

                        <div class=""form-group row"">
                            <label for=""ImageURL"" class=""col-sm-4"">Image URL</label>
                            <div class=""col-sm-8"">
                               ");
                WriteLiteral(@" <textarea class=""form-control"" id=""ImageURL"" name=""ImageURL"" rows=""3""></textarea>
                            </div>
                        </div>

                        <div class=""form-group row"">
                            <label for=""SampleURL"" class=""col-sm-4"">Sample URL</label>
                            <div class=""col-sm-8"">
                                <textarea class=""form-control"" id=""SampleURL"" name=""SampleURL"" rows=""3""></textarea>
                            </div>
                        </div>

                        <hr />

                        <p>
                            <button class=""btn btn-outline-primary"" type=""submit"">Save</button>
                        </p>
                    </fieldset>
                ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
            </div>
        </div>
    </div>
</div>

<div id=""divAudio"" style=""display:none;"">
    <audio controls autoplay>
        <source class=""srcMP3"" type=""audio/mpeg"">
        Your browser does not support the audio element.
    </audio>
</div>

<script type=""text/javascript"">

    $('#ReleaseDate').on('click', function () {
        $('.datepicker').pickadate({
            selectMonths: true,
            selectYears: true,
            close: ''
        }).pickadate('picker').start();
        $('.datepicker').addClass('picker__input--active');
    });

    $('#ReleaseDate').on('change', function () {
        $('.datepicker').pickadate().pickadate('picker').stop();
    });

    $(document).ready(function () {
        $('#cover-load').show();
        bindArtists();
    });

    function bindArtists() {
        $.ajax({
            url: ""https://localhost:44330/api/values"",
            type: 'GET',
            dataType: 'json',
            success: function (p) {
   ");
            WriteLiteral(@"             $('#tblArtists').bootstrapTable(""destroy"").bootstrapTable({
                    data: p,
                    pagination: true,
                    //pageNumber
                    //count
                    sortable: true,
                    pageSize: 10,
                    pageList: [10, 25, 50, 75]
                });
                $('#cover-load').hide();
            }
        });
    }

    function NumberFormatter(value, row, index) {
        return index + 1 + '.';
    }

    function ImgFormatter(value, row, index) {
        return '<div class=""row""><img class=""col-md-6 image-url"" src=""' + value + '"" /><p class=""col-md-6"">' + row.AlbumName + '</p>';
    }

    function MusicFormatter(value, row, index) {
        return '<button type=""button"" class=""btnPlay""></button>';
    }

    function dateFormat(value, row, index) {
        if (value != null)
            return moment(value).format('DD MMM YYYY');
        else
            return '-';
    }

    fun");
            WriteLiteral(@"ction ActionFormatter(value, row, index) {
        return '<button type=""button"" class=""btn btn-outline-success btnEdit"">Edit</button>' +
            '<button type=""button"" class=""btn btn-outline-danger btnDelete"">Delete</button>';
    }

    window.PlayEvents = {
        'click .btnPlay': function (e, value, row, index) {
            $('#exampleModalCenter1').modal('toggle');
            $('.srcMP3').attr(""src"", value);

            $('#audioBody').html($('#divAudio').html());
            $('#spArtistName').text(row.ArtistName);
            $('#spAlbumName').text(row.AlbumName);
            $('#imgAlbum').attr(""src"", row.ImageURL);
        }
    }

    $('#exampleModalCenter1').on('hidden.bs.modal', function () {
        $('audio').each(function () {
            this.pause(); // Stop playing
            this.currentTime = 0; // Reset time
        }); 
    });

    $("".btnAdd"").click(function (e) {
        e.stopPropagation();
        $('#addModal').modal('toggle');
        resetFor");
            WriteLiteral(@"m();
    });

    function resetForm() {
        $('#ArtistID').val(0);
        $('#ArtistName').val('');
        $('#AlbumName').val('');
        $('#Price').val('');
        $('#ImageURL').val('');
        $('#SampleURL').val('');
        $('.datepicker').pickadate().pickadate('picker').close();
        $('.datepicker').pickadate('set').set('select', null);
    }

    window.ActionEvents = {
        'click .btnEdit': function (e, value, row, index) {
            $('#addModal').modal('toggle');
            $('#ArtistID').val(row.ArtistID);
            $('#ArtistName').val(row.ArtistName);
            $('#AlbumName').val(row.AlbumName);
            $('#Price').val(row.Price);
            $('#ImageURL').val(row.ImageURL);
            $('#SampleURL').val(row.SampleURL);

            var rd = moment(row.ReleaseDate).format('YYYY/MM/DD');
            $('.datepicker').pickadate({
                selectMonths: true,
                selectYears: true
            }).pickadate('picker').clo");
            WriteLiteral(@"se();
            $('.datepicker').pickadate('set').set('select', new Date(rd));
        },
        'click .btnDelete': function (e, value, row, index) {
            $('#cover-load').show();
            if (confirm(""Are you sure want to delete this row?"")) {
                $.ajax({
                    url: ""https://localhost:44330/api/values/"" + row.ArtistID,
                    type: 'DELETE',
                    dataType: 'json',                // <---update this
                    success: function (p) {
                        alert('Delete Success');
                        bindArtists();
                    },
                    error: function (e) {
                        alert(""Error"");
                        console.log(e);
                        $('#cover-load').hide();
                    }
                });
            }
            else {
                $('#cover-load').hide();
            }
        },
    }


    $('#addForm').ajaxForm({
        dataType: 'j");
            WriteLiteral(@"son',
        data: $('#addForm').serialize(),
        beforeSubmit: function () {
            $('#cover-load').show();
        },
        success: function (p) {
            $('#addModal').modal('toggle');
            $(""#addForm"")[0].reset();
            bindArtists();
            alert(""Success"");
            resetForm();
            $('#cover-load').hide();
        },
        error: function (e) {
            alert(""Error"");
            console.log(e);
            $('#cover-load').hide();
        }
    });
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
